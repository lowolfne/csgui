﻿namespace Tango
{
	using System;
	using System.Text;
	
	public static class TimeUtils
	{
		public static string DynamicTimeFormat( TimeSpan timeSpan )
		{
			StringBuilder str = new StringBuilder();
			
			if ( timeSpan.Days > 0 )
			{
				str.Append( string.Format( "{0} D ", timeSpan.Days ) );
			}
			
			if ( timeSpan.Hours > 0 )
			{
				str.Append( string.Format( "{0} H ", timeSpan.Hours ) );
			}
			
			if ( timeSpan.Minutes > 0 )
			{
				str.Append( string.Format( "{0} M ", timeSpan.Minutes ) );
			}
			
			str.Append( string.Format( "{0} S", timeSpan.Seconds ) );
			
			return str.ToString();
		}
		
		public static string DynamicTimerFormat( TimeSpan timeSpan )
		{
			StringBuilder str = new StringBuilder();
			str.Append( string.Format( "{0:00}'{1:00}\"{2:000}", timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds ) );
			
			return str.ToString();
		}
	}
}
