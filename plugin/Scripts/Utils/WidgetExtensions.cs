﻿namespace Tango
{
	using System;
	
    // todo document
	public static class WidgetExtensions
	{
		public static void Subscribe( this GenericButtonWidget button, IContextProvider provider, Action onClick, bool isInteractable = true )
		{
			if ( button != null )
			{
				ButtonContext context = new ButtonContext
				{
					IsInteractable = isInteractable,
					OnButtonClicked = onClick
				};
				
				button.BindContext( provider, context );
			}
		}
		
		public static void Subscribe( this TimerWidget displayTime, IContextProvider provider, DateTime dateTime, string format )
		{
			if ( displayTime != null )
			{
				TimerContext context = new TimerContext
				{
					Type = TimerType.DisplayDateTime,
					Current = dateTime,
					TimeFormat = format
				};
				
				displayTime.BindContext( provider, context );
			}
		}
		
		public static void Subscribe( this TimerWidget timer, IContextProvider provider, double seconds, Action onTimesUp, bool isDynamic = true )
		{
			if ( timer != null )
			{
				timer.Subscribe( provider, DateTime.Now, DateTime.Now.AddSeconds( seconds ), onTimesUp, isDynamic );
			}
		}
		
		public static void Subscribe( this TimerWidget timer, IContextProvider provider, DateTime startTime, DateTime endTime, Action onTimesUp, bool isDynamic = true )
		{
			if ( timer != null )
			{
				TimerContext context = new TimerContext
				{
					Type = TimerType.TimerDecrement,
					Start = startTime,
					End = endTime,
					OnTimesUp = onTimesUp,
					IsDyanmicFormat = isDynamic
				};
				
				timer.BindContext( provider, context );
			}
		}
		
		public static void Subscribe( this TimerWidget displayCurrentTime, IContextProvider provider, string format )
		{
			if ( displayCurrentTime != null )
			{
				TimerContext context = new TimerContext
				{
					Type = TimerType.DisplayCurrentTime,
					TimeFormat = format
				};
				
				displayCurrentTime.BindContext( provider, context );
			}
		}
		
		public static void Subscribe( this ProgressWidget progressWidget, IContextProvider provider, float current, float max, string format, bool isInteractable )
		{
			if ( progressWidget != null )
			{
				ProgressContext context = new ProgressContext
				{
					Current = current,
					Max = max,
					Format = format,
					IsInteractable = isInteractable
				};
				
				progressWidget.BindContext( provider, context );
			}
		}
		
		public static void Subscribe( this InputListenerWidget inputListenerWidget, IContextProvider provider, bool isInteractable, Action onListen )
		{
			if ( inputListenerWidget != null )
			{
				InputListenerContext context = new InputListenerContext()
				{
					IsInteractable = isInteractable,
					OnListen = onListen
				};
				
				inputListenerWidget.BindContext( provider, context );
			}
		}
		
		public static void Subscribe( this ImageWidget imageWidget, IContextProvider provider, string path, string file, bool isInResourcesFolder )
		{
			if ( imageWidget != null )
			{
				ImageWidgetContext context = new ImageWidgetContext()
				{
					Path = path,
					Name = file,
					IsInResources = isInResourcesFolder
				};
				
				imageWidget.BindContext( provider, context );
			}
		}
	}
}