﻿namespace Tango
{
	using UnityEngine;
	
	[RequireComponent( typeof( NestedWidget ) )]
	public abstract class ViewController<T> : MonoViewController, IViewController<T>
	{
		#region [ Unity Methods ]
		
		private void Awake()
		{
			_nestedWidget = GetComponent<NestedWidget>();
		}
		
		#endregion
		
		#region [ UMonoViewController Overrides ]
		
		protected T _context;
		new public T Context
		{
			get
			{
				return _context;
			}
		}
		
		public override bool BindContext( IContextProvider provider, IContext context )
		{
			if ( context == null )
			{
				return false;
			}
			
			base.BindContext( provider, context );
			
			if ( context.Equals( _context ) )
			{
				return true;
			}
			
			ReleaseContext();
			_context = ( T )context;
			
			return true;
		}
		
		public override bool ReleaseContext()
		{
			base.ReleaseContext();
			_context = default( T );
			
			return true;
		}
		
		#endregion
	}
}