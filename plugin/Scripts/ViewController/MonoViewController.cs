﻿namespace Tango
{
	using System;
	using UnityEngine;
	
	public partial class MonoViewController : MonoBehaviour, IViewController<object>
	{
		[SerializeField]
		private string _name = null;
		
		// todo implement basic view animations
		//[SerializeField]
		//private Animator _animator = null;
		
		protected NestedWidget _nestedWidget = null;
	}
}