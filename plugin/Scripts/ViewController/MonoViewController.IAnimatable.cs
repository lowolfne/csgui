﻿namespace Tango
{
	public partial class MonoViewController
	{
		// show animate begins
		public virtual void OnAnimateShowBegin()
		{
		
		}
		// show animate ends
		public virtual void OnAnimateShowEnd()
		{
		
		}
		// hide animate begins
		public virtual void OnAnimateHideBegin()
		{
		
		}
		// hide animate ends
		public virtual void OnAnimateHideEnd()
		{
		
		}
	}
}