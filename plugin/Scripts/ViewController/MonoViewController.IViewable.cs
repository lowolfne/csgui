﻿namespace Tango
{
	using System;
	
	public partial class MonoViewController
	{
		#region [ IViewable Implementation ]
		
		protected long _id;
		public long Id
		{
			get
			{
				if ( string.IsNullOrEmpty( _name ) )
				{
					throw new Exception( "View Controller name cannot be null." );
				}
				
				
				if ( _id == 0 )
				{
					_id = _name.GetHashCode();
				}
				
				return _id;
			}
		}
		
		public virtual void OnAboutToShow()
		{
            // experimental
            OnAnimateShowBegin();
		}
		
		public virtual bool Show()
		{
			gameObject.SetActive( true );
			Refresh();
			return true;
		}
		
		public virtual void OnDidShow()
		{
		}
		
		
		public virtual void OnAboutToHide()
		{
            // experimental
            OnAnimateHideBegin();
		}
		
		public virtual bool Hide()
		{
			return true;
		}
		
		public virtual void OnDidHide()
		{
			gameObject.SetActive( false );
		}
		
		#endregion
		
		#region [ IRefreshable Implementation ]
		
		public virtual void Refresh()
		{
			bool shouldRefresh = _nestedWidget != null && _nestedWidget.Widgets != null;
			
			if ( shouldRefresh )
			{
				MonoWidget[] widgets = _nestedWidget.Widgets;
				
				for ( int i = 0; i < widgets.Length; ++i )
				{
					widgets[i].Refresh();
				}
			}
		}
		
		#endregion
		
		#region [ IBindable Implementation ]
		
		protected object _rawContext;
		public object Context
		{
			get
			{
				return _rawContext;
			}
		}
		
		public IContextProvider Provider
		{
			get
			{
				return null;
			}
		}
		
		public virtual bool BindContext( IContextProvider provider, IContext context )
		{
			_rawContext = context;
			
			return true;
		}
		
		public virtual void OnDidBindContext()
		{
		}
		
		public virtual void OnDidReleaseContext()
		{
		}
		
		public virtual bool ReleaseContext()
		{
			_rawContext = null;
			
			bool shouldClearWidgetContext = _nestedWidget != null && _nestedWidget.Widgets != null;
			
			if ( shouldClearWidgetContext )
			{
				MonoWidget[] widgets = _nestedWidget.Widgets;
				
				for ( int i = 0; i < widgets.Length; ++i )
				{
					widgets[i].ReleaseContext();
				}
			}
			
			return true;
		}
		
		#endregion
		
		#region [ ITickable Implementation ]
		
		private ITickable[] _widgets;
		public ITickable[] Widgets
		{
			get
			{
				if ( _widgets == null )
				{
					_widgets = GetComponentsInChildren<MonoWidget>();
				}
				
				return _widgets;
			}
		}
		
		#endregion
	}
}