﻿namespace Tango
{
	using System;
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.EventSystems;
	
	[Serializable]
	public class ButtonContext : IContext
	{
		public bool IsInteractable;
		public Action OnButtonClicked;
		
		public string Value
		{
			get
			{
				return string.Format( "[ IsInteractable: {0}] [ OnButtonClicked: {1} ]", IsInteractable, OnButtonClicked.ToString() );
			}
		}
	}
	
	// todo add semaphore, anti spam feature or timer
	[RequireComponent( typeof( Button ) )]
	public class GenericButtonWidget : Widget<ButtonContext>, IPointerEnterHandler, IPointerDownHandler
	{
		[SerializeField]
		private Button _button = null;
		
		[SerializeField]
		private AudioSource _audioSource = null;
		
		[SerializeField]
		private AudioClip _onClickSfx = null;
		
		[SerializeField]
		private AudioClip _onHoverSfx = null;
		
		private bool _isInteractable = false;
		public bool IsInteractable
		{
			get
			{
				return _isInteractable;
			}
			set
			{
				if ( _button != null )
				{
					_button.interactable = value;
				}
				
				_isInteractable = value;
			}
		}
		
		#region [ Unity Methods ]
		
		private void OnEnable()
		{
			if ( _button != null )
			{
				_button.onClick.AddListener( OnButtonClicked );
			}
		}
		
		private void OnDisable()
		{
			if ( _button != null )
			{
				_button.onClick.RemoveListener( OnButtonClicked );
			}
		}
		
		public void OnPointerEnter( PointerEventData ped )
		{
			if ( _audioSource != null && _onHoverSfx != null )
			{
				_audioSource.clip = _onHoverSfx;
				_audioSource.Play();
			}
		}
		
		public void OnPointerDown( PointerEventData ped )
		{
			if ( _audioSource != null && _onClickSfx != null )
			{
				_audioSource.clip = _onClickSfx;
				_audioSource.Play();
			}
		}
		
		#endregion
		
		#region [ Widget Methods ]
		
		public override void OnDidBindContext()
		{
			base.OnDidBindContext();
			
			if ( _button == null )
			{
				try
				{
					_button = GetComponent<Button>();
				}
				catch
				{
					throw new Exception( "There is no Unity::Button Component." );
				}
			}
			
			if ( _button != null )
			{
				_button.interactable = _context.IsInteractable;
			}
		}
		
		private void OnButtonClicked()
		{
			if ( _context != null && _context.IsInteractable )
			{
				_context.OnButtonClicked.Invoke();
			}
		}
		
		#endregion
	}
}
