﻿namespace Tango
{
    // DateTime format
    // https://www.c-sharpcorner.com/blogs/date-and-time-format-in-c-sharp-programming1
    // Timespan format
    // ex "mm\\:ss\\:ff"
    // https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-timespan-format-strings

    using System;
	using UnityEngine;
	using UnityEngine.UI;
	
	using TMPro;
	
	public enum TimerType
	{
		DisplayCurrentTime, // displays the current date and time
		DisplayDateTime, // displays a specified date and time
        DisplayTImerResult,
		TimerIncrement, //
		TimerDecrement, //
        TimerDisplay
	}
	
	[Serializable]
	public class TimerContext : IContext
	{
		public DateTime Start;
		public DateTime End;
		public DateTime Current;
		
		public TimerType Type;
		public Action OnTimesUp;
		
		public float Delay; // delay amount before timer starts ( in seconds )
		public string TimeFormat;
		public bool IsDyanmicFormat;
		public bool ShouldStartCountdown;

        // experimental
        public TimeSpan TotalTime;
		
		public TimeSpan TimeLeft
		{
			get
			{
				return End - Start;
			}
		}
		
		public string Value
		{
			get
			{
				return string.Format( "[Type: {0}]", Type );
			}
		}
	}
	
	public class TimerWidget : Widget<TimerContext>
	{
		[SerializeField]
		private Text _text = null;
		
		[SerializeField]
		private TextMeshProUGUI _tmpText = null;
		
		private double _elapseTime = 0.0;
		public double ElapseTime
		{
			get
			{
				return _elapseTime;
			}
		}
		
		private double _timeLeft = 0.0;
		private float _delayCountdown = 0f;
		private Action _onTimesUp = delegate { };
		
		public override void OnDidBindContext()
		{
			base.OnDidBindContext();
			
			_elapseTime = 0;
			_delayCountdown = Context.Delay;
			_timeLeft = Context.TimeLeft.TotalSeconds;
			
			// todo tidy up
			if ( Context.Type == TimerType.DisplayDateTime )
			{
				if ( _text != null )
				{
					_text.text = Context.Current.ToString( Context.TimeFormat );
				}
				
				if ( _tmpText != null )
				{
					_tmpText.text = Context.Current.ToString( Context.TimeFormat );
				}
			}
			
			// todo tidy up
			if ( Context.Type == TimerType.TimerDecrement )
			{
				string timeLeft = _timeLeft.ToString( "0" );
				
				if ( _text != null )
				{
					_text.text = timeLeft;
				}
				
				if ( _tmpText != null )
				{
					_tmpText.text = timeLeft;
				}
			}

            // todo tidy up
            if ( Context.Type == TimerType.DisplayTImerResult )
            {
                string totalSeconds = Context.TotalTime.ToString( Context.TimeFormat );//string.Format( "{mm\\:ss\\.ff}", Context.TotalTime ); // Context.TotalTime.ToString( "c" );

                if ( _text != null )
                {
                    _text.text = totalSeconds;//Context.TotalTime.ToString( "c" );
                }

                if ( _tmpText != null )
                {
                    _tmpText.text = totalSeconds;//Context.TotalTime.ToString( "c" );
                }
            }
			
			_onTimesUp = Context.OnTimesUp;
		}
		
		// experimental
		// extends timer with a specified amount of seconds
		public bool TryExtendTimeBySeconds( float seconds )
		{
			if ( Context != null )
			{
				_timeLeft += seconds;
				return true;
			}
			
			return false;
		}
		
		
		// experimental
		public void Reset()
		{
			TimeSpan timeSpan = TimeSpan.FromSeconds( 0 );
			
			switch ( Context.Type )
			{
				case TimerType.TimerIncrement:
					{
						if ( _text != null )
						{
							_text.text = TimeUtils.DynamicTimerFormat( timeSpan );
						}
						
						if ( _tmpText != null )
						{
							_tmpText.SetText( TimeUtils.DynamicTimerFormat( timeSpan ) );
						}
					}
					break;
			}
		}
		
		public override bool ReleaseContext()
		{
			_elapseTime = 0;
			_timeLeft = 0;
			_delayCountdown = 0;
			_onTimesUp = null;
			return base.ReleaseContext();
		}
		
		public override void Tick( float deltaTime )
		{
			base.Tick( deltaTime );
			
			if ( Context == null || !IsTickable )
			{
#if VERBOSE
				Debug.LogError( string.Format( "Context is null at {0}", gameObject.name ) );
#endif
				return;
			}
			
			if ( _delayCountdown > 0 )
			{
				_delayCountdown -= deltaTime;
				return;
			}
			
			_elapseTime += deltaTime;
			
			switch ( Context.Type )
			{
				case TimerType.TimerDecrement:
					{
						if ( _timeLeft > 0 )
						{
							TimeSpan timeSpan = TimeSpan.FromSeconds( _timeLeft );
							
							string timerText = string.Empty;
							
							if ( Context.IsDyanmicFormat )
							{
								timerText = TimeUtils.DynamicTimeFormat( timeSpan );
							}
							else
							{
								timerText = timeSpan.TotalSeconds.ToString( "0" );
							}
							
							if ( _text != null )
							{
								_text.text = timerText;
							}
							
							if ( _tmpText != null )
							{
								_tmpText.text = timerText;
							}
							
							_timeLeft -= deltaTime;
						}
						else
						{
							if ( _onTimesUp != null )
							{
								_onTimesUp.Invoke();
								_onTimesUp = null;
							}
						}
					}
					break;
					
				case TimerType.TimerIncrement:
					{
						TimeSpan timeSpan = TimeSpan.FromSeconds( _timeLeft );
						
						// todo timer style functionality
						if ( _text != null )
						{
							_text.text = TimeUtils.DynamicTimerFormat( timeSpan );
						}
						
						if ( _tmpText != null )
						{
							_tmpText.SetText( TimeUtils.DynamicTimerFormat( timeSpan ) );
						}
						
						_timeLeft += deltaTime;
					}
					break;
					
					
				case TimerType.DisplayCurrentTime:
				default:
					{
						// todo support functionality to use UTC
						if ( _text != null )
						{
							_text.text = DateTime.Now.ToString( Context.TimeFormat );
						}
						
						if ( _tmpText != null )
						{
							_tmpText.SetText( DateTime.Now.ToString( Context.TimeFormat ) );
						}
					}
					break;
			}
		}
	}
}
