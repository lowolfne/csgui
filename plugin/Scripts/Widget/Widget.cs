﻿namespace Tango
{
	public abstract class Widget<T> : MonoWidget, IWidget<T>
	{
		protected T _context;
		new public T Context
		{
			get
			{
				return _context;
			}
		}
		
		public override bool BindContext( IContextProvider provider, IContext context )
		{
			if ( context == null )
			{
				ReleaseContext();
				return false;
			}
			
			base.BindContext( provider, context );
			
			if ( context.Equals( _context ) )
			{
				OnDidBindContext();
				return true;
			}
			
			ReleaseContext();
			_context = ( T )context;
			OnDidBindContext();
			
			return true;
		}
		
		public override bool ReleaseContext()
		{
			base.ReleaseContext();
			
			_context = default( T );
			OnDidReleaseContext();
			
			return true;
		}
	}
	
}