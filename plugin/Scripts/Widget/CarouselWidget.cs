﻿namespace Tango
{
	// todo add support for scrollbars
	using System;
	using UnityEngine;
	using UnityEngine.UI;
	using System.Collections.Generic;
	
	public enum CarouselType
	{
		Horizontal,
		Vertical
	}
	
	public class CarouselContext : IContext
	{
		public IList< ICarouselItemContext > Items;
		public CarouselType Type;
		
		public string Value
		{
			get
			{
				return string.Format( "[Carousel Count: {0}][Type: {1}]", Items != null ? Items.Count : 0, Type );
			}
		}
	}
	
	public class CarouselWidget : Widget< CarouselContext >, ICarousel, IContextProvider
	{
		[SerializeField]
		private RectTransform _carouselItemPrefab = null;
		
		// todo only use padding left and top
		[SerializeField]
		private RectOffset _padding = null;
		
		[SerializeField]
		private float _spacing = 1.0f;
		
		[SerializeField]
		private RectTransform _content = null;
		
		[SerializeField]
		private ScrollRect _scrollRect = null;
		
		[SerializeField]
		private CarouselType _type = CarouselType.Horizontal;
		
		[SerializeField]
		private List<MonoWidget> _recycledCells;
		
		[SerializeField]
		private Scrollbar _scrollbarVertical = null;
		
		[SerializeField]
		private Scrollbar scrollbarHorizontal = null;
		
		private bool _isInitialise = false;
		private float _itemSize = 0f;
		private int _additionalCacheItems = 2;
		private int _itemsPerScreen = 0;
		private float _viewportWidth = 0f;
		private float _viewportHeight = 0f;
		private Vector2 _onScrolledValue = Vector2.zero;
		
		// indexing
		private int _index = 1;
		private int _firstItemIndex = 0;
		private int _lastItemIndex = 0;
		
		// bounds
		private float _leftBounds = 0f;
		private float _rightBounds = 0f;
		private float _topBounds = 0f;
		private float _bottomBounds = 0f;
		
		#region [ Widget Override Methods ]
		
		public override void OnDidBindContext()
		{
			base.OnDidBindContext();
			
			// todo move this to Widget or IWidget
			_isInitialise = true;
			
			if ( _scrollRect == null || _content == null || _carouselItemPrefab == null )
			{
				throw new System.Exception( "[ScrollRect || Content || Carousel Prefab ] can not be null." );
			}
			
			_scrollRect.onValueChanged.RemoveListener( OnScrollChanged );
			_scrollRect.onValueChanged.AddListener( OnScrollChanged );
			_viewportWidth = _scrollRect.viewport.rect.width;
			_viewportHeight = _scrollRect.viewport.rect.height;
			
			// todo tidy up
			if ( _scrollbarVertical != null )
			{
				_scrollbarVertical.onValueChanged.RemoveListener( OnScrollbarChanged );
				_scrollbarVertical.onValueChanged.AddListener( OnScrollbarChanged );
			}
			
			SetDimensions();
			
			SetContentSize();
			
			InitialiseContents();
		}
		
		public override bool ReleaseContext()
		{
			if ( _scrollRect != null )
			{
				_scrollRect.onValueChanged.RemoveListener( OnScrollChanged );
			}
			
			if ( _scrollbarVertical != null )
			{
				_scrollbarVertical.onValueChanged.RemoveListener( OnScrollbarChanged );
			}
			
			if ( _recycledCells != null )
			{
				for ( int i = 0; i < _recycledCells.Count; ++i )
				{
					_recycledCells[i].ReleaseContext();
					Destroy( _recycledCells[i].gameObject );
				}
				
				_recycledCells.Clear();
			}
			
			if ( _isInitialise )
			{
				Reset();
			}
			
			_isInitialise = false;
			
			return base.ReleaseContext();
		}
		
		#endregion
		
		#region [ ICarousel Implementation ]
		
		public virtual bool IsInitialise
		{
			get
			{
				return _isInitialise;
			}
		}
		
		public virtual void SetDimensions()
		{
			const float HALF = 0.5f;
			const int ADD_ONE = 1;
			
			// set the dimensions
			if ( _type == CarouselType.Horizontal )
			{
				_itemSize = _carouselItemPrefab.rect.width;
				_itemsPerScreen = Convert.ToInt32( _viewportWidth / ( _itemSize + _spacing ) );
				
				_leftBounds = ( ( _itemsPerScreen * HALF + ADD_ONE ) * ( _itemSize + _spacing ) ) + _padding.left;
				_rightBounds = -( ( _itemsPerScreen * HALF + ADD_ONE ) * ( _itemSize + _spacing ) ) + _padding.right;
			}
			else
			{
				_itemSize = _carouselItemPrefab.rect.height;
				_itemsPerScreen = Convert.ToInt32( _viewportHeight / ( _itemSize + _spacing ) );
				
				_topBounds = ( ( _itemsPerScreen * HALF + ADD_ONE ) * ( _itemSize + _spacing ) ) + _padding.top;
				_bottomBounds = -( ( _itemsPerScreen * HALF + ADD_ONE ) * ( _itemSize + _spacing ) ) + _padding.bottom;
			}
		}
		
		// set the content size
		public virtual void SetContentSize()
		{
			float contentSize = 0f;
			
			int realCount = Context.Items.Count;
			
			// todo tidy up
			if ( _type == CarouselType.Horizontal )
			{
				_content.localPosition = new Vector3( 0, 0, 0 );
				contentSize = ( ( realCount ) * ( _itemSize ) + ( ( realCount - 1 ) * _spacing ) ) + _padding.left + _padding.right;
				_content.sizeDelta = new Vector2( contentSize, _content.rect.height );
			}
			else
			{
				contentSize = ( ( realCount ) * ( _itemSize ) + ( ( realCount - 1 ) * _spacing ) ) + _padding.top + _padding.bottom;
				_content.sizeDelta = new Vector2( _content.rect.width, contentSize );
				
				// set content position to start on top
				_content.localPosition = new Vector2( _content.localPosition.x, -contentSize );
			}
		}
		
		public virtual void Reset()
		{
			float contentSize = 0f;
			int realCount = Context.Items.Count;
			
			// todo tidy up
			if ( _type == CarouselType.Horizontal )
			{
				_content.localPosition = new Vector3( 0, 0, 0 );
			}
			else
			{
				contentSize = ( ( realCount ) * ( _itemSize ) + ( ( realCount - 1 ) * _spacing ) ) + _padding.top + _padding.bottom;
				_content.localPosition = new Vector2( _content.localPosition.x, -contentSize );
			}
		}
		
		// initialise the contents including the additional widgets
		public virtual void InitialiseContents()
		{
			_carouselItemPrefab.gameObject.SetActive( true );
			
			int count = 0;
			
			// calculate if we need to spawn additional widgets
			if ( Context.Items.Count <= _itemsPerScreen + 1 )
			{
				count = Context.Items.Count;
			}
			else //( Context.Items.Count > _itemsPerScreen )
			{
				count = _itemsPerScreen + _additionalCacheItems;
			}
			
			if ( _recycledCells == null )
			{
				_recycledCells = new List<MonoWidget>();
			}
			
			RectTransform prevItem = null;
			
			if ( _type == CarouselType.Horizontal )
			{
				for ( int i = 0; i < count; ++i )
				{
					RectTransform item = Instantiate( _carouselItemPrefab, _content );
					MonoWidget carouselItem = item.GetComponent<MonoWidget>();
					
					if ( i == 0 )
					{
						item.localPosition = new Vector3( _padding.left, _padding.top, 0 );
						prevItem = item;
					}
					else
					{
						item.localPosition = new Vector3( prevItem.localPosition.x + _itemSize + _spacing, _padding.top, 0 );
						prevItem = item;
					}
					
					if ( i < Context.Items.Count )
					{
						carouselItem.BindContext( this, Context.Items[i] );
					}
					
					_recycledCells.Add( carouselItem );
				}
			}
			else
			{
				// for vertical contents
				for ( int i = 0; i < count; ++i )
				{
					RectTransform item = Instantiate( _carouselItemPrefab, _content );
					MonoWidget carouselItem = item.GetComponent<MonoWidget>();
					
					if ( i == 0 )
					{
						item.localPosition = new Vector3( _padding.left, _content.sizeDelta.y - _itemSize - _padding.top, 0 );
						prevItem = item;
					}
					else
					{
						item.localPosition = new Vector3( _padding.left, ( prevItem.localPosition.y - _itemSize - _spacing ), 0 );
						prevItem = item;
					}
					
					if ( i < Context.Items.Count )
					{
						carouselItem.BindContext( this, Context.Items[i] );
					}
					
					_recycledCells.Add( carouselItem );
				}
			}
			
			_firstItemIndex = 0;
			_lastItemIndex = count - 1;
			_index = _lastItemIndex;
			
			_carouselItemPrefab.gameObject.SetActive( false );
		}
		
		// todo tidy up
		// recalculate the scroll elements
		public virtual void Recalculate( Vector2 value )
		{
			if ( _type == CarouselType.Horizontal )
			{
				// scroll left
				if ( _scrollRect.velocity.normalized.x > 0 )
				{
					bool isLastItemInRightBounds = GetItemPosition( _recycledCells[_lastItemIndex].transform ) < ( _rightBounds );
					
					if ( isLastItemInRightBounds && _index > ( _itemsPerScreen + 1 ) )
					{
						Vector3 firstItemPosition = _recycledCells[ _firstItemIndex ].transform.localPosition;
						_recycledCells[ _lastItemIndex ].transform.localPosition = new Vector3( firstItemPosition.x - _itemSize - _spacing, _padding.top, 0 );
						--_index;
						
						int binding = _index - _itemsPerScreen - 1;
#if VERBOSE
						Debug.Log( "Binded index: " + binding );
#endif
						// bind widget context
						_recycledCells[ _lastItemIndex ].ReleaseContext();
						_recycledCells[ _lastItemIndex ].BindContext( this, Context.Items[binding] );
						
						_firstItemIndex = _lastItemIndex;
						
						if ( _lastItemIndex > 0 )
						{
							--_lastItemIndex;
						}
						else
						{
							_lastItemIndex = _recycledCells.Count - 1;
						}
					}
				}
				// scroll right
				else if ( _scrollRect.velocity.normalized.x < 0 )
				{
					bool isFirstItemInLeftBounds = GetItemPosition( _recycledCells[_firstItemIndex].transform ) > ( _leftBounds );
					
					if ( isFirstItemInLeftBounds && ( _index < Context.Items.Count - 1 ) )
					{
						++_index;
						
						Vector3 lastItemPosition = _recycledCells[ _lastItemIndex ].transform.localPosition;
						_recycledCells[ _firstItemIndex ].transform.localPosition = new Vector3( lastItemPosition.x + _itemSize + _spacing, _padding.top, 0 );
						
						// bind widget context
						_recycledCells[ _firstItemIndex ].ReleaseContext();
						_recycledCells[_firstItemIndex].BindContext( this, Context.Items[ _index] );
#if VERBOSE
						Debug.Log( "Binding index: " + _index );
#endif
						_lastItemIndex = _firstItemIndex;
						
						if ( _firstItemIndex < _recycledCells.Count - 1 )
						{
							++_firstItemIndex;
						}
						else
						{
							_firstItemIndex = 0;
						}
					}
				}
				
				//_currentNormalisedPosition = value.x;
			}
			else
			{
				// scrolling to top
				if ( _scrollRect.velocity.normalized.y < 0 )
				{
					bool isLastItemInBottomBounds = GetItemPosition( _recycledCells[_lastItemIndex].transform ) < ( _bottomBounds );
					
					if ( isLastItemInBottomBounds && _index > ( _itemsPerScreen + 1 ) )
					{
						Vector3 firstItemPosition = _recycledCells[ _firstItemIndex ].transform.localPosition;
						_recycledCells[ _lastItemIndex ].transform.localPosition = new Vector3( _padding.left, firstItemPosition.y + _itemSize + _spacing, 0 );
						--_index;
						
						int binding = _index - _itemsPerScreen - 1;
#if VERBOSE
						Debug.Log( "Binded index: " + binding );
#endif
						// bind widget context
						_recycledCells[ _lastItemIndex ].ReleaseContext();
						_recycledCells[ _lastItemIndex ].BindContext( this, Context.Items[binding] );
						
						_firstItemIndex = _lastItemIndex;
						
						if ( _lastItemIndex > 0 )
						{
							--_lastItemIndex;
						}
						else
						{
							_lastItemIndex = _recycledCells.Count - 1;
						}
					}
				}
				// scrolling to bottom
				else if ( _scrollRect.velocity.normalized.y > 0 )
				{
					bool isFirstItemInTopBounds = GetItemPosition( _recycledCells[_firstItemIndex].transform ) > ( _topBounds );
					
					if ( isFirstItemInTopBounds && ( _index < Context.Items.Count - 1 ) )
					{
						++_index;
						
						Vector3 lastItemPosition = _recycledCells[ _lastItemIndex ].transform.localPosition;
						_recycledCells[ _firstItemIndex ].transform.localPosition = new Vector3( _padding.left, lastItemPosition.y - _itemSize - _spacing, 0 );
						
						// bind widget context
						_recycledCells[_firstItemIndex].ReleaseContext();
						_recycledCells[_firstItemIndex].BindContext( this, Context.Items[_index] );
#if VERBOSE
						Debug.Log( "Binding index: " + _index );
#endif
						_lastItemIndex = _firstItemIndex;
						
						if ( _firstItemIndex < _recycledCells.Count - 1 )
						{
							++_firstItemIndex;
						}
						else
						{
							_firstItemIndex = 0;
						}
					}
				}
				
				//_currentNormalisedPosition = value.y;
			}
		}
		
		public virtual void ScrollToIndex( int index )
		{
			// todo
		}
		
		public virtual void ScrollToPosition( float position )
		{
			// todo
		}
		
		public virtual void OnScrollChanged( Vector2 value )
		{
			if ( !_isInitialise )
			{
				return;
			}
			
			Recalculate( value );
		}
		
		public virtual void OnScrollbarChanged( float value )
		{
			if ( !_isInitialise )
			{
				return;
			}
			
			// todo add support for scrollbars
			Recalculate( Vector3.zero );
		}
		
		public virtual float GetItemPosition( Transform item )
		{
			if ( _type == CarouselType.Horizontal )
			{
				return -item.localPosition.x - _content.localPosition.x;
			}
			else if ( _type == CarouselType.Vertical )
			{
				return item.localPosition.y + _content.localPosition.y;
			}
			
			return 0f;
		}
		
		#endregion
	}
}