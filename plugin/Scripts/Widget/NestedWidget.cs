﻿namespace Tango
{
	using UnityEngine;
	
	[ExecuteInEditMode]
	public class NestedWidget : MonoBehaviour
	{
		[SerializeField]
		private Component[] _components;
		public Component[] Components
		{
			get
			{
				return _components;
			}
		}
		
		[SerializeField]
		private MonoWidget[] _widgets;
		public MonoWidget[] Widgets
		{
			get
			{
				return _widgets;
			}
		}
		
		private void Awake()
		{
			_widgets = GetComponentsInChildren<MonoWidget>();
			_components = GetComponentsInChildren<Component>();
		}
	}
}
