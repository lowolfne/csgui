﻿namespace Tango
{
	using UnityEngine;
	
	public interface ICarousel
	{
		bool IsInitialise
		{
			get;
		}
		void ScrollToIndex( int index );
		void ScrollToPosition( float position );
		void OnScrollChanged( Vector2 value );
		void SetDimensions();
		void SetContentSize();
		void InitialiseContents();
		void Recalculate( Vector2 value );
		void Reset();
		float GetItemPosition( Transform item );
	}
}
