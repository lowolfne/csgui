﻿namespace Tango
{
	using System;
	using UnityEngine;
	using UnityEngine.UI;
	
	using TMPro;
	
	[Serializable]
	public class ProgressContext : IContext
	{
		public bool IsInteractable;
		public string Format;
		public float Current;
		public float Max;
		public float Normalise
		{
			get
			{
				return ( Current / Max );
			}
		}
		
		public string Value
		{
			get
			{
				return string.Format( "[Current: {0}][Max: {1}][Normalise: {2}]", Current, Max, Normalise );
			}
		}
	}
	
	public class ProgressWidget : Widget<ProgressContext>
	{
		[SerializeField]
		protected Slider _slider = null;
		
		[SerializeField]
		protected Image _image = null;
		
		[SerializeField]
		protected Text _text = null;
		
		[SerializeField]
		protected TextMeshProUGUI _tmpText = null;
		
		public override void OnDidBindContext()
		{
			base.OnDidBindContext();
			
			if ( _slider != null )
			{
				_slider.interactable = Context.IsInteractable;
				_slider.maxValue = Context.Max;
				_slider.value = Context.Current;
				_slider.normalizedValue = Context.Normalise;
			}
			
			if ( _image != null )
			{
				_image.fillAmount = Context.Normalise;
			}
			
			float current = Context.Current > Context.Max ? Context.Max : Context.Current;
			RefreshTextDisplay( current, Context.Max, Context.Format );
		}
		
		protected void RefreshTextDisplay( float current, float max, string textFormat )
		{
			string format = textFormat;
			
			if ( string.IsNullOrEmpty( textFormat ) )
			{
				format = "{0}/{1}";
			}
			
			if ( _text != null )
			{
				_text.text = string.Format( textFormat, current, max );
			}
			
			if ( _tmpText != null )
			{
				_tmpText.SetText( string.Format( textFormat, current, max ) );
			}
		}
	}
}