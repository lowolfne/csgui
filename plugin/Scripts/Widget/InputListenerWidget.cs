﻿namespace Tango
{
	// Note: This should only be used in the UI features
	
	using System;
	
	public class InputListenerContext : IContext
	{
		public bool IsInteractable;
		public Action OnListen;
		
		public string Value
		{
			get
			{
				return string.Format( "[IsInterable: {0}][OnListen: {1}]", IsInteractable, OnListen.ToString() );
			}
		}
	}
	
	public class InputListenerWidget : Widget< InputListenerContext >
	{
		public override void Tick( float deltaTime )
		{
			base.Tick( deltaTime );
			
			Context.OnListen.Invoke();
		}
	}
}