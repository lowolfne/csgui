﻿namespace Tango
{
	public interface ICarouselItemContext : IContext {}
	public abstract class CarouselItemWidget<T> : Widget< T > {}
}