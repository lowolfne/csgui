﻿namespace Tango
{
	using UnityEngine;
	public class MonoWidget : MonoBehaviour, IWidget<object>
	{
		[SerializeField]
		private bool _isTickable = false;
		
		#region [ IWidget Implementation ]
		
		protected object _rawContext;
		public object Context
		{
			get
			{
				return _rawContext;
			}
		}
		
		protected IContextProvider _provider;
		public IContextProvider Provider
		{
			get
			{
				return _provider;
			}
		}
		
		public bool IsTickable
		{
			get
			{
				return _isTickable;
			}
			set
			{
				_isTickable = value;
			}
		}
		
		public virtual bool BindContext( IContextProvider provider, IContext context )
		{
			_provider = provider;
			_rawContext = context;
			
			return true;
		}
		
		public virtual void OnDidBindContext()
		{
		}
		
		public virtual void OnDidReleaseContext()
		{
		}
		
		public virtual void Refresh()
		{
		}
		
		public virtual bool ReleaseContext()
		{
			_rawContext = null;
			_provider = null;
			
			return true;
		}
		
		public virtual void Tick( float deltaTime )
		{
			if ( _rawContext == null )
			{
				return;
			}
		}
		
		#endregion
	}
}