﻿namespace Tango
{
	using System;
	using UnityEngine;
	using UnityEngine.UI;
	
	[Serializable]
	public sealed class ImageWidgetContext : IContext
	{
		public string Name;
		public string Path;
		public bool IsInResources;
		
		public string Value
		{
			get
			{
				return string.Format( "[Path: {0}][File: {1}]", Name, Path );
			}
		}
	}
	
	// todo add functionality to download image
	public class ImageWidget : Widget<ImageWidgetContext>
	{
		[SerializeField]
		private Image _image = null;
		
		[SerializeField]
		private RawImage _rawImage = null;
		
		[SerializeField]
		private bool _isInResources = false;
		
		public override void OnDidBindContext()
		{
			base.OnDidBindContext();
			
			_isInResources = Context.IsInResources;
			
			if ( _image != null )
			{
				if ( Context.IsInResources )
				{
					Sprite sprite = Resources.Load<Sprite>( string.Format( "{0}/{1}", Context.Path, Context.Name ) );
					_image.sprite = sprite;
				}
				else
				{
					// do nothing for now
				}
			}
			
			if ( _rawImage != null )
			{
				// do nothing for now
			}
		}
	}
}
