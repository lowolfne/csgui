﻿namespace Tango
{
	using UnityEngine;
	
	public class Director : MonoBehaviour, IDirector
	{
		[SerializeField]
		protected string _viewName = null;
		
		[SerializeField]
		protected GameObject _viewPrefab = null;
		
		[SerializeField]
		protected GameObject _parentObject = null;
		
		[SerializeField]
		protected GameObject _instance = null;
		
		#region [ Private Variables ]
		
		protected long _id = -1;
		private MonoViewController _viewController = null;
		
		#endregion
		
		// called at the start
		protected virtual void Initialise()
		{
			if ( _id == -1 )
			{
				_id = _viewName.GetHashCode();
			}
			
			UViewHub.TryAddDirector( _id, this );
		}
		
		#region [ Unity Methods ]
		
		protected virtual void Awake()
		{
			UViewEvents.OnPrepareEnterViewEvent -= OnPrepareEnterView;
			UViewEvents.OnPrepareEnterViewEvent += OnPrepareEnterView;
			
			UViewEvents.OnEnterViewEvent -= OnEnterView;
			UViewEvents.OnEnterViewEvent += OnEnterView;
			
			UViewEvents.OnPrepareExitViewEvent -= OnPrepareExitView;
			UViewEvents.OnPrepareExitViewEvent += OnPrepareExitView;
			
			UViewEvents.OnExitViewEvent -= OnExitView;
			UViewEvents.OnExitViewEvent += OnExitView;
			
			UViewEvents.OnInitialiseEvent -= Initialise;
			UViewEvents.OnInitialiseEvent += Initialise;
		}
		
		protected virtual void OnDestroy()
		{
			UViewEvents.OnInitialiseEvent -= Initialise;
			UViewEvents.OnPrepareEnterViewEvent -= OnPrepareEnterView;
			UViewEvents.OnEnterViewEvent -= OnEnterView;
			UViewEvents.OnPrepareExitViewEvent -= OnPrepareExitView;
			UViewEvents.OnExitViewEvent -= OnExitView;
		}
		
		#endregion
		
		#region[ IDirector Implementation ]
		
		public IViewable ViewController
		{
			get
			{
				return _viewController;
			}
		}
		
		public virtual void SetView()
		{
		
			if ( _instance == null )
			{
				_instance = Instantiate( _viewPrefab );
			}
			
			if ( _viewController == null )
			{
				_viewController = _instance.GetComponent<MonoViewController>();
			}
			
			if ( _parentObject != null && _instance != null )
			{
				_instance.transform.SetParent( _parentObject.transform );
			}
		}
		
		public virtual void DestroyView()
		{
			_viewController = null;
			_id = -1;
			Destroy( _instance );
		}
		
		#endregion
		
		#region [ ViewEvents Callbacks ]
		
		public virtual bool OnPrepareEnterView( long current, long previous, object context, UViewHistory history, UViewContexts contexts, out int result )
		{
			result = ( int ) EResult.Success;
			return true;
		}
		
		public virtual bool OnEnterView( long current, long previous, object context, UViewHistory history, UViewContexts contexts, out int result )
		{
			result = ( int )EResult.Success;
			return true;
		}
		
		public virtual bool OnPrepareExitView( long next, long current, UViewHistory history, UViewContexts contexts, out int result )
		{
			result = ( int )EResult.Success;
			return true;
		}
		
		public virtual bool OnExitView( long next, long current, UViewHistory history, UViewContexts contexts, out int result )
		{
			result = ( int )EResult.Success;
			return true;
		}
		
		#endregion
	}
}