﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

class UViewEditorData
{
public bool IsSelected;
public string ViewName;
public string Comment;
}

public class UViewGenerator : EditorWindow
{
private string m_selectedFile;
private string m_filePath;
private List<UViewEditorData> m_views = new List<UViewEditorData>();
	private Vector2 m_scrollPos = new Vector2();
	
	
	[MenuItem( "Window/Tango/Generate Views..." )]
	static void Open()
	{
		GetWindow<UViewGenerator>( true, "UViewGenerator", true );
	}
	
	private void OnGUI()
	{
		GUILayout.Space( 15 );
		
		EditorGUILayout.BeginHorizontal();
		
		GUILayout.Label( "File", GUILayout.Width( position.width * 0.035f ) );
		
		m_selectedFile = GUILayout.TextArea( m_selectedFile );
		
		if ( GUILayout.Button( "Save", GUILayout.Width( position.width * 0.15f ) ) )
		{
			if ( string.IsNullOrEmpty( m_selectedFile ) )
			{
				EditorUtility.DisplayDialog( "Warning", "File name cannot be null or empty.", "Close" );
			}
			else
			{
				m_filePath = EditorUtility.SaveFolderPanel( "Save view config.", "", "" );
				GenerateSaveFile( m_filePath );
			}
		}
		
		if ( GUILayout.Button( "Load", GUILayout.Width( position.width * 0.15f ) ) )
		{
			m_filePath = EditorUtility.OpenFilePanel( "Select File", "", "g.cs" );
			
			if ( m_filePath.Length != 0 )
			{
				m_selectedFile = Path.GetFileNameWithoutExtension( m_filePath );
				m_selectedFile = m_selectedFile.Substring( 0, m_selectedFile.Length - 2 );
				string[] lines = File.ReadAllLines( m_filePath );
				
				DigestData( lines );
			}
		}
		
		EditorGUILayout.EndHorizontal();
		
		m_scrollPos = GUILayout.BeginScrollView( m_scrollPos );
		
		GUILayout.Space( 15 );
		
		GUILayout.BeginHorizontal();
		
		GUILayout.Label( "View Id", EditorStyles.boldLabel );
		GUILayout.Label( "Description", EditorStyles.boldLabel );
		GUILayout.EndHorizontal();
		
		GUILayout.BeginVertical();
		
		GUILayout.Space( 5 );
		
		if ( m_views != null )
		{
			int count = m_views.Count;
			
			for ( int i = 0; i < count; ++i )
			{
				GUILayout.BeginHorizontal();
				m_views[i].IsSelected = GUILayout.Toggle( m_views[i].IsSelected, string.Empty, GUILayout.Width( position.width * 0.015f ) );
				m_views[i].ViewName = GUILayout.TextField( m_views[i].ViewName, GUILayout.Width( position.width * 0.45f ) );
				m_views[i].Comment = GUILayout.TextField( m_views[i].Comment );
				GUILayout.EndHorizontal();
			}
		}
		
		GUILayout.EndVertical();
		
		GUILayout.EndScrollView();
		
		GUILayout.Space( 15 );
		
		if ( GUILayout.Button( "Add" ) )
		{
			m_views.Add( new UViewEditorData() );
		}
		
		if ( GUILayout.Button( "Remove" ) )
		{
			RemoveSelectedViews();
		}
		
		if ( GUILayout.Button( "Clear" ) )
		{
			m_views.Clear();
		}
		
		if ( GUILayout.Button( "Generate" ) )
		{
			if ( string.IsNullOrEmpty( m_selectedFile ) )
			{
				EditorUtility.DisplayDialog( "Warning", "File name cannot be null or empty.", "Close" );
			}
			else
			{
				m_filePath = EditorUtility.SaveFolderPanel( "Save Generate View Ids to folder", "", "" );
				GenerateCSharpFile( m_filePath );
			}
		}
	}
	
	private void DigestData( string[] lines )
	{
		if ( m_views != null )
		{
			m_views.Clear();
			
			for ( int i = 0; i < lines.Length; ++i )
			{
				string line = lines[i];
				line = line.Substring( 2, line.Length - 2 );
				
				string[] items = line.Split( ',' );
				
				UViewEditorData viewData = new UViewEditorData()
				{
					ViewName = items[0],
					Comment = items[2],
				};
				
				m_views.Add( viewData );
			}
		}
	}
	
	private void RemoveSelectedViews()
	{
		int count = m_views.Count;
		
		List<UViewEditorData> newList = new List<UViewEditorData>();
		
		for ( int i = 0; i < count; ++i )
		{
			if ( !m_views[i].IsSelected )
			{
				newList.Add( m_views[i] );
			}
		}
		
		m_views = newList;
	}
	
	private void GenerateSaveFile( string filePath )
	{
		const string format = "//{0},{1},{2}\n";
		const string fileFormat = ".g.cs";
		
		string contents = string.Empty;
		
		for ( int i = 0; i < m_views.Count; ++i )
		{
			string item = string.Format( format, m_views[i].ViewName, m_views[i].ViewName.GetHashCode(), m_views[i].Comment );
			contents += item;
		}
		
		File.WriteAllText( filePath + "/" + m_selectedFile + fileFormat, contents );
	}
	
	private void GenerateCSharpFile( string filePath )
	{
		const string fileFormat = ".cs";
		
		string description = "//------------------------------------------------\n" +
		                     "// Auto-generated do not edit\n" +
		                     "//\n" +
		                     "//\n" +
		                     "// Use the Window/Tango/View Id Generator\n" +
		                     "//\n" +
		                     "// Last Updated: {0}\n" +
		                     "//------------------------------------------------\n";
		                     
		description = string.Format( description, System.DateTime.Now );
		
		string contents = description;
		
		string start = "public class GENERATED_VIEW_ID\n";
		contents += start;
		
		string startParanthesis = "{\n";
		contents += startParanthesis;
		
		string format = "\t\tpublic const int {0} = {1}; //{2}\n";
		
		for ( int i = 0; i < m_views.Count; ++i )
		{
			string item = string.Format( format, m_views[i].ViewName, m_views[i].ViewName.GetHashCode(), m_views[i].Comment );
			contents += item;
		}
		
		string endParenthesis = "}";
		contents += endParenthesis;
		
		File.WriteAllText( filePath + "/" + m_selectedFile + fileFormat, contents );
	}
}
#endif