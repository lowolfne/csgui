﻿namespace Tango
{
	public interface IAnimatable
	{
		// show animate begins
		void OnAnimateShowBegin();
		// show animate ends
		void OnAnimateShowEnd();
		// hide animate begins
		void OnAnimateHideBegin();
		// hide animate ends
		void OnAnimateHideEnd();
	}
}