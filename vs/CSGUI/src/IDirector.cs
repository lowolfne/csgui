﻿namespace Tango
{
	public interface IDirector
	{
		IViewable ViewController
		{
			get;
		}
		
		void SetView();
		
		void DestroyView();
	}
}
