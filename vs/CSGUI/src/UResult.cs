﻿namespace Tango
{
    /// <summary>
    /// Results
    /// </summary>
	public enum EResult
	{
		Ignore = -1,    /// ignores the results
		Fail,           /// fail
		Success         /// success
	}
}
