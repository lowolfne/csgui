﻿namespace Tango
{
	public interface IContextReceiver<T>
	{
		T Context
		{
			get;
		}
	}
}
