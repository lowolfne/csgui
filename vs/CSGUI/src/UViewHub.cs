﻿namespace Tango
{
	using System.Collections.Generic;
	
	public static class UViewHub
	{
		private const int VIEW_LIMIT = 25;
		
		private static UViewHistory _history;
		private static UViewContexts _contexts;
		private static Dictionary<long, IViewable> _views;
		private static Dictionary<long, IDirector> _directors;
		private static IViewable _currentView;
		private static bool _hasBind = false;
		
		/// <summary>
        /// Initialises the gui system.
        /// </summary>
		public static void Initialise()
		{
			_currentView = null;
			_history = new UViewHistory( 15 );
			_contexts = new UViewContexts();
			_views = new Dictionary<long, IViewable>( 15 );
			_directors = new Dictionary<long, IDirector>( 15 );
			
			UViewEvents.TriggerOnInitialise();
		}
		
        /// <summary>
        /// Adds the specified director.
        /// </summary>
        /// <param name="id">view id</param>
        /// <param name="director">director</param>
        /// <returns>Returns TRUE, if successful. FALSE, otherwise.</returns>
		public static bool TryAddDirector( long id, IDirector director )
		{
			IViewable viewController = null;
			
			if ( _directors != null )
			{
				_directors[id] = director;
				director.SetView();
				viewController = director.ViewController;
				
				if ( viewController.Hide() )
				{
					// todo I think this should be called in the animator
					viewController.OnDidHide();
				}
			}
			
			if ( _views != null )
			{
				_views[id] = viewController;
			}
			
			return true;
		}
		
        /// <summary>
        /// Rebinds the context to a specific view.
        /// </summary>
        /// <param name="id">view id</param>
        /// <param name="context">view context</param>
        /// <returns>Returns TRUE, if successful. FALSE, otherwise.</returns>
		public static bool Rebind( long id, IContext context )
		{
			if ( _currentView == null || _currentView.Id != id )
			{
				return false;
			}
			
			_currentView.BindContext( null, context );
			_currentView.Refresh();
			
			return true;
		}
		
        /// <summary>
        /// Switch to the next specified view id.
        /// </summary>
        /// <param name="id">view id</param>
        /// <param name="context">view context</param>
        /// <param name="shouldAddToHistory">If TRUE, adds view to history. FALSE, otherwise.</param>
        /// <returns></returns>
		public static bool TriggerNextView( long id, IContext context = null, bool shouldAddToHistory = true )
		{
			if ( _history == null )
			{
				return false;
			}
			
			int result = ( int )EResult.Success;
			
			_contexts.Add( id, context );
			
			if ( _currentView != null )
			{
				if ( UViewEvents.OnPrepareExitViewDelegate != null )
				{
					UViewEvents.OnPrepareExitViewDelegate.Invoke(
					    id,
					    _currentView.Id,
					    _history,
					    _contexts,
					    out result );
				}
				
				if ( result == ( int )EResult.Fail )
				{
					return false;
				}
				
				_currentView.OnAboutToHide();
				
				_currentView.ReleaseContext();
				_hasBind = false;
				
				if ( _currentView.Hide() )
				{
					// todo I think this should be called in the animator
					_currentView.OnDidHide();
				}
				
				if ( UViewEvents.OnExitViewDelegate != null )
				{
					UViewEvents.OnExitViewDelegate.Invoke(
					    id,
					    _currentView.Id,
					    _history,
					    _contexts,
					    out result );
				}
				
				if ( result == ( int )EResult.Fail )
				{
					return false;
				}
			}
						
			if ( _views.Count >= VIEW_LIMIT )
			{
				// do nothing for now
				//_views.Remove( _history.History[0] );
				// todo destroy view implementation
			}
			
			IViewable view;
			
			if ( !_views.TryGetValue( id, out view ) )
			{
				IDirector director;
				
				if ( !_directors.TryGetValue( id, out director ) )
				{
					return false;
				}
				
				director.SetView();
				view = director.ViewController;
			}
			
			_currentView = view;
			
			if ( UViewEvents.OnPrepareEnterViewDelegate != null )
			{
				UViewEvents.OnPrepareEnterViewDelegate.Invoke(
				    id,
				    _history.PreviousView,
				    context,
				    _history,
				    _contexts,
				    out result );
			}
			
			if ( result == ( int )EResult.Fail )
			{
				return false;
			}
			
			if ( view.BindContext( null, context ) )
			{
				view.OnDidBindContext();
				_hasBind = true;
			}
			else
			{
				view.ReleaseContext();
				_hasBind = false;
				
				return false;
			}
			
			view.OnAboutToShow();
			
			if ( view.Show() )
			{
				// todo I think this should be called in the animator
				view.OnDidShow();
			}
			
			if ( UViewEvents.OnEnterViewDelegate != null )
			{
				UViewEvents.OnEnterViewDelegate.Invoke(
				    id,
				    _history.PreviousView,
				    context,
				    _history,
				    _contexts,
				    out result );
				    
			}
			
			if ( result == ( int )EResult.Fail )
			{
				return false;
			}
			
			if ( shouldAddToHistory )
			{
				_history.Push( id );
			}
			
			return true;
		}
		
        /// <summary>
        /// Switch back to the previous view.
        /// </summary>
        /// <returns>Returns TRUE, if successful. FALSE, otherwise.</returns>
		public static bool TriggerBackToLastView()
		{
			return TriggerBackToLastView( null );
		}

        /// <summary>
        /// Switches back to a specified view. (EXPERIMENTAL)
        /// </summary>
        /// <param name="id">view id</param>
        /// <returns>Returns TRUE, if successful. FALSE, otherwise.</returns>
        public static bool TriggerBackTo( long id )
		{
            if ( _history == null && _contexts == null )
            {
                return false;
            }

            long currentView = _history.BackTo( id );

            IContext context;
            _contexts.TryGetRawContext( _history.PreviousView, out context );

            long previousViewId = _history.Pop();
            TriggerNextView( previousViewId, context, false );

            return false;
		}

        /// <summary>
        /// Switches back to the previous view that is not in the ignore views.
        /// </summary>
        /// <param name="ignoreViews">ignored views</param>
        /// <returns>Returns TRUE, if successful. FALSE, otherwise.</returns>
        public static bool TriggerBackToLastView( params long[] ignoreViews )
		{			
			if ( _history == null && _contexts == null )
			{
				return false;
			}
			
			long currentView = _history.SkipViews( ignoreViews );
			
			IContext context;
			
			_contexts.TryGetRawContext( _history.PreviousView, out context );
			
			long previousViewId = _history.Pop();
			
			TriggerNextView( previousViewId, context, false );
			
			return true;
		}
		
        /// <summary>
        /// Main Loop.
        /// </summary>
        /// <param name="deltaTime">delta time</param>
		public static void Loop( float deltaTime )
		{
			if ( !_hasBind )
			{
				return;
			}
			
			if ( _currentView != null && _currentView.Widgets != null )
			{
				for ( int i = 0; i < _currentView.Widgets.Length; ++i )
				{
					ITickable tickable = _currentView.Widgets[i];
					
					if ( tickable.IsTickable )
					{
						tickable.Tick( deltaTime );
					}
				}
			}
		}
		
        /// <summary>
        /// Clear caches.
        /// </summary>
		public static void Clear()
		{
			if ( _currentView != null )
			{
				_currentView.ReleaseContext();
			}
			
			if ( _history != null )
			{
				_history.Clear();
			}
			
			if ( _contexts != null )
			{
				_contexts.Clear();
			}
			
			if ( _views != null )
			{
				_views.Clear();
			}
			
			if ( _directors != null )
			{
				_directors.Clear();
			}
		}
	}
}
