﻿namespace Tango
{
	public interface IWidget<T> : IContextReceiver<T>, IBindable, IRefreshable, ITickable
	{
	}
}
