﻿namespace Tango
{
	// rename to IViewContext
	public interface IContext
	{
		// context value must be readable
		string Value
		{
			get;
		}
	}
}
