﻿namespace Tango
{
	public interface IViewController<T> : IContextReceiver<T>, IContextProvider, IViewable, IAnimatable//, IReactive
	{
	}
}
