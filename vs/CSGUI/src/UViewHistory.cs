﻿namespace Tango
{
	using System.Collections.Generic;
	
	public class UViewHistory
	{
		private List<long> _history;
		private List<int> _removeViewsByIndex;
		
        /// <summary>
        /// Current View
        /// </summary>
		public long CurrentView
		{
			get
			{
				if ( _history != null && _history.Count > 0 )
				{
					int current = _history.Count - 1;
					return _history[current];
				}
				
				return 0;
			}
		}
		
        /// <summary>
        /// Previous View
        /// </summary>
		public long PreviousView
		{
			get
			{
				if ( _history != null && _history.Count > 1 )
				{
					int previous = _history.Count - 2;
					return _history[previous];
				}
				
				return CurrentView;
			}
		}
		
        /// <summary>
        /// View History
        /// </summary>
		public long[] History
		{
			get
			{
				if ( _history != null )
				{
					_history.ToArray();
				}
				
				return null;
			}
		}

        // experimental
        /// <summary>
        /// Switches back to a specified view id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public long BackTo( long id )
        {
            if ( _removeViewsByIndex != null )
            {
                _removeViewsByIndex.Clear();
            }

            for ( int i = _history.Count - 1; i > 0; --i )
            {
                long current = _history[i];

                if ( current != id )
                {
                    _removeViewsByIndex.Add( i );
                }
                else if ( current == id )
                {
                    break;
                }
            }

            for ( int i = 0; i < _removeViewsByIndex.Count; ++i )
            {
                _history.RemoveAt( _removeViewsByIndex[i] );
            }

            return CurrentView;
        }
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="skipViews"></param>
        /// <returns></returns>
		public long SkipViews( params long[] skipViews )
		{
			if ( _removeViewsByIndex != null )
			{
				_removeViewsByIndex.Clear();
			}
			
			if ( skipViews != null )
			{
				for ( int i = 0; i < skipViews.Length; ++i )
				{
					long id = skipViews[i];
					
					for ( int j = 0; j < _history.Count; ++j )
					{
						if ( _history[j] == id )
						{
							_removeViewsByIndex.Add( j );
							break;
						}
					}
				}
				
				for ( int i = 0; i < _removeViewsByIndex.Count; ++i )
				{
					_history.RemoveAt( _removeViewsByIndex[i] );
				}
			}
			
			return CurrentView;
		}
		
        /// <summary>
        /// Clear.
        /// </summary>
		public void Clear()
		{
			if ( _history != null )
			{
				_history.Clear();
			}
		}
		
		internal UViewHistory() { }
		internal UViewHistory( int capacity )
		{
			_history = new List<long>( capacity );
			_removeViewsByIndex = new List<int>( capacity );
		}
	
		internal void Push( long id )
		{
			if ( _history != null && !_history.Contains( id ) )
			{
				_history.Add( id );
			}
		}
		
		internal long Pop()
		{
			if ( _history != null )
			{
				int index = _history.Count - 1;
				long id = PreviousView;
				
				if ( index > 0 )
				{
					_history.RemoveAt( index );
				}
				
				return id;
			}
			
			return -1;
		}
	}
}
