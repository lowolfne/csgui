﻿namespace Tango
{
	using System.Collections.Generic;
	
	public class UViewContexts
	{
		private Dictionary<long, IContext> _contexts;
		
		internal UViewContexts()
		{
			_contexts = new Dictionary<long, IContext>( 20 );
		}
		
		internal void Add( long id, IContext context )
		{
			if ( _contexts != null )
			{
				_contexts[ id ] = context;
			}
		}

        /// <summary>
        /// Retrieves a specified context from a specified view id.
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <param name="id">view id</param>
        /// <param name="context">view context</param>
        /// <returns>Returns TRUE, if successful. FALSE, otherwise.</returns>
        public bool TryGetContext<T>( long id, out T context )
		{
			IContext temp;
			context = default( T );
			
			if ( TryGetRawContext( id, out temp ) )
			{
				context = ( T )temp;
				return ( context != null );
			}
			
			return false;
		}

        /// <summary>
        /// Retrieves a raw specified context from a specified view id.
        /// </summary>
        /// <param name="id">view id</param>
        /// <param name="context">view context</param>
        /// <returns>Returns TRUE, if successful. FALSE, otherwise.</returns>
        public bool TryGetRawContext( long id, out IContext context )
		{
			context = null;
			
			if ( _contexts != null )
			{
				return _contexts.TryGetValue( id, out context );
			}
			
			return false;
		}

        internal bool TryRemoveContext( long id )
		{
			IContext context;
			
			if ( _contexts != null && _contexts.TryGetValue( id, out context ) )
			{
				return _contexts.Remove( id );
			}
			
			return false;
		}
		
		internal void Clear()
		{
			if ( _contexts != null )
			{
				_contexts.Clear();
			}
		}
	}
}
