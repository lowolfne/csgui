﻿namespace Tango
{
	using System;
	
	public static class UViewEvents
	{
		#region [ View Hub Callbacks ]
		
		//=======================================================================
		// On Initialise
		//=======================================================================
		
		internal static Action OnInitialiseDelegate;
		public static event Action OnInitialiseEvent
		{
			add
			{
				OnInitialiseDelegate -= value;
				OnInitialiseDelegate += value;
			}
			
			remove
			{
				OnInitialiseDelegate -= value;
			}
		}
		
		internal static void TriggerOnInitialise()
		{
			if ( OnInitialiseDelegate != null )
			{
				OnInitialiseDelegate.Invoke();
			}
		}
		
		public delegate bool EnterViewDelegate( long nextViewId, long prevViewId, object context, UViewHistory history, UViewContexts historyArgs, out int result );
		public delegate bool ExitViewDelegate( long nextViewId, long prevViewId, UViewHistory history, UViewContexts contexts, out int result );
		
		//=======================================================================
		// Prepare to enter
		//=======================================================================
		
		internal static EnterViewDelegate OnPrepareEnterViewDelegate;
		public static event EnterViewDelegate OnPrepareEnterViewEvent
		{
			add
			{
				OnPrepareEnterViewDelegate -= value;
				OnPrepareEnterViewDelegate += value;
			}
			
			remove
			{
				OnPrepareEnterViewDelegate -= value;
			}
		}
		
		//=======================================================================
		// enter view
		//=======================================================================
		
		internal static EnterViewDelegate OnEnterViewDelegate;
		public static event EnterViewDelegate OnEnterViewEvent
		{
			add
			{
				OnEnterViewDelegate -= value;
				OnEnterViewDelegate += value;
			}
			
			remove
			{
				OnEnterViewDelegate -= value;
			}
		}
		
		//=======================================================================
		// prepare to exit
		//=======================================================================
		
		internal static ExitViewDelegate OnPrepareExitViewDelegate;
		public static event ExitViewDelegate OnPrepareExitViewEvent
		{
			add
			{
				OnPrepareExitViewDelegate -= value;
				OnPrepareExitViewDelegate += value;
			}
			
			remove
			{
				OnPrepareExitViewDelegate -= value;
			}
		}
		
		//=======================================================================
		// exit view
		//=======================================================================
		
		internal static ExitViewDelegate OnExitViewDelegate;
		public static event ExitViewDelegate OnExitViewEvent
		{
			add
			{
				OnExitViewDelegate -= value;
				OnExitViewDelegate += value;
			}
			
			remove
			{
				OnExitViewDelegate -= value;
			}
		}
		
		#endregion
	}
}
