﻿namespace Tango
{
	public interface ITickable
	{
		bool IsTickable
		{
			get;
		}
		
		void Tick( float deltaTime );
	}
}
