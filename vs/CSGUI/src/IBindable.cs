﻿namespace Tango
{
	public interface IBindable
	{
		IContextProvider Provider
		{
			get;
		}
		
		bool BindContext( IContextProvider provider, IContext context );
		
		void OnDidBindContext();
		
		bool ReleaseContext();
		
		void OnDidReleaseContext();
	}
}
