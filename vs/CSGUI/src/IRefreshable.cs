﻿namespace Tango
{
	public interface IRefreshable
	{
		void Refresh();
	}
}
