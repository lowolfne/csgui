﻿namespace Tango
{
	public interface IViewable : IBindable, IRefreshable, ITickableContainer
	{
		long Id
		{
			get;
		}
		
		void OnAboutToShow();
		
		bool Show();
		
		void OnDidShow();
		
		void OnAboutToHide();
		
		bool Hide();
		
		void OnDidHide();
	}
}
