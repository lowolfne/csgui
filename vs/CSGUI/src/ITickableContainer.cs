﻿namespace Tango
{
	public interface ITickableContainer
	{
		ITickable[] Widgets
		{
			get;
		}
	}
}

